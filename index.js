import express from "express";
import axios from "axios";
import cheerio from "cheerio";
const PORT = process.env.PORT || 8000;
const app = express();

const url = 'https://www.pluralsight.com'

const urlBlog = 'https://www.pluralsight.com/blog/';

const paths = [
  {
    path: 'technology',
    url: 'https://www.pluralsight.com/blog/technology'
  },
  {
    path: 'software-development',
    url: 'https://www.pluralsight.com/blog/software-development'
  },
  {
    path: 'data-professional',
    url: 'https://www.pluralsight.com/blog/data-professional'
  },
  {
    path: 'it-ops',
    url: 'https://www.pluralsight.com/blog/it-ops'
  },
  {
    path: 'security-professional',
    url: 'https://www.pluralsight.com/blog/security-professional'
  }
]
const sampleArticles = [];
const allArticles = [];


//Welcome 
app.get('/', (req, res) => {
  res.json('Welcome to my API for all Pluralsight articles')
});


//Sample articles

axios.get(urlBlog)
  .then((response) => {
    const html = response.data;
    const $ = cheerio.load(html);


    $('.hub-tile', html).each(function () {
      const articleTitle = $(this).find('h4').text();
      const articleUrl = url + $(this).find('a').attr('href');


      sampleArticles.push({
        articleTitle,
        articleUrl,
      })
    })
    
  }).catch(err => console.log(err))

app.get('/samples', (req, res) => {
  res.json(sampleArticles)
})

//All articles

paths.forEach((section) => {
  axios.get(section.url)
    .then((response) => {
      const html = response.data;
      const $ = cheerio.load(html);

      $('.hub-tile', html).each(function () {
        const articleTitle = $(this).find('h4').text();
        const articleUrl = url + $(this).find('a').attr('href');


        allArticles.push({
          articleTitle,
          articleUrl,
        })
      })
    }).catch((err) => console.log(err))
})



app.get('/articles', (req, res) => {
  res.json(allArticles)
});

//Section articles

app.get('/articles/:pathId', (req, res) => {

  const pathId = req.params.pathId;
  const pathUrl = urlBlog + pathId;
  // console.log(pathUrl)
  axios.get(pathUrl)
    .then((response) => {
      const html = response.data;
      const $ = cheerio.load(html);
      const sectionArticles = [];

      $('.hub-tile', html).each(function () {
        const articleTitle = $(this).find('h4').text();
        const articleUrl = url + $(this).find('a').attr('href');

        sectionArticles.push({
          articleTitle,
          articleUrl,
        })
      })
      res.json(sectionArticles)
    })
})

app.listen(PORT, () => console.log(`Server running on PORT ${PORT}`));
